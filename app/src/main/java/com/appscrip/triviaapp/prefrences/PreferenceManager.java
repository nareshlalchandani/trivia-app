package com.appscrip.triviaapp.prefrences;

import android.content.Context;
import android.content.SharedPreferences;

import com.appscrip.triviaapp.utils.AppConstant;

/**
 * Created by naresh chandani
 */
public class PreferenceManager {

    private final static String SHARE = AppConstant.PROJECT_NAME;

    private static final String PREFERENCE_SAVED = "c1";

    private static final String FIELD_NAME = "Name";

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Context context;

    public PreferenceManager(Context context) {

        this.context = context;
        preferences = context.getSharedPreferences(SHARE, Context.MODE_PRIVATE);
    }

    public void setPreference(boolean isSaved) {
        editor = preferences.edit();
        editor.putBoolean(PREFERENCE_SAVED, isSaved);
        editor.apply();
    }

    public String getName() {
        return preferences.getString(FIELD_NAME, null);
    }

    public void setName(String userType) {
        editor = preferences.edit();
        editor.putString(FIELD_NAME, userType);
        editor.apply();
    }

    //Clear all store preferences data
    public void logout() {
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}
