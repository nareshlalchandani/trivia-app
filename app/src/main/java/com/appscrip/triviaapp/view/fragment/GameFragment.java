package com.appscrip.triviaapp.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.adapter.ViewPagerAdapter;
import com.appscrip.triviaapp.database.DatabaseHelper;
import com.appscrip.triviaapp.database.Tables.AnswerTable;
import com.appscrip.triviaapp.model.AnswerModel;
import com.appscrip.triviaapp.model.QuestionModel;
import com.appscrip.triviaapp.model.QuizModel;
import com.appscrip.triviaapp.utils.JsonUtil;
import com.appscrip.triviaapp.utils.LogUtility;
import com.appscrip.triviaapp.utils.ToastUtils;
import com.appscrip.triviaapp.utils.widget.NonSwipeableViewPager;
import com.appscrip.triviaapp.view.activity.FinishGameActivity;
import com.appscrip.triviaapp.view.base.BaseFragment;
import com.google.gson.Gson;

public class GameFragment extends BaseFragment {

    public static final String TAG = GameFragment.class.getSimpleName();

    public static final String SESSION_ID = "questionModel";

    NonSwipeableViewPager viewPager;
    Button btnBack;
    Button btnAction;

    int viewpagerCount = 0;
    int currentPage = 0;

    int sessionId;

    private ViewPagerAdapter adapter;

    public static GameFragment newInstance(int sessionId) {

        //Create instance
        GameFragment fragment = new GameFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(SESSION_ID, sessionId);//Pass data in bundle
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_game, container, false);
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Get data from bundle object
        sessionId = getArguments().getInt(SESSION_ID);

        //Init views
        viewPager = rootView.findViewById(R.id.viewpager);
        btnBack = rootView.findViewById(R.id.btn_back);
        btnAction = rootView.findViewById(R.id.btn_action);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                --currentPage;//Decrease by 1
                viewPager.setCurrentItem(currentPage, true);//Change current page
            }
        });

        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Get current loaded fragment
                QuestionFragment fragment = (QuestionFragment) adapter.getItem(viewPager.getCurrentItem());

                //Get selected answer
                AnswerModel model = fragment.getAnswer();

                //Check is answer is selected or not
                if (model.getAnswer() == null) {
                    ToastUtils.shortToast("Select answer");
                    return;
                }

                //Insert answer into answer table
                AnswerTable.saveAnswer(sessionId, model, DatabaseHelper.getInstance(getContext()));

                //Check is view pager is at last index or not, if yes then next time launch final submit activity
                if (currentPage == (viewpagerCount - 1)) {

                    Intent intent = new Intent(getContext(), FinishGameActivity.class);
                    intent.putExtra(FinishGameActivity.SESSION_ID, sessionId);
                    startActivity(intent);

                } else {
                    ++currentPage;//Increase by 1
                }

                viewPager.setCurrentItem(currentPage, true);//Change current page
            }
        });

        addTabs(viewPager);//Load data into view pager

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {

                currentPage = i;//To identify current page position

                //Update UI
                updateBackButtonVisibility();
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Update UI
        updateBackButtonVisibility();
    }

    private void addTabs(ViewPager viewPager) {

        //Convert json file to string
        String quizListData = JsonUtil.loadJSONFromAsset(getContext(), "quizList.json");

        //Deserialize json to object
        QuizModel quizModel = new Gson().fromJson(quizListData, QuizModel.class);
        viewpagerCount = quizModel.getData().size();//Get data size

        //Create instance of view pager adapter
        adapter = new ViewPagerAdapter(getChildFragmentManager());

        //load fragment into view pager
        for (QuestionModel questionModel : quizModel.getData()) {

            String dataModel = new Gson().toJson(questionModel);

            adapter.addFrag(QuestionFragment.newInstance(dataModel), "Multiple");
        }

        viewPager.setAdapter(adapter);
    }

    private void updateBackButtonVisibility() {

        //LogUtility.printDebugMsg("Current Page : " + currentPage);

        if (currentPage == 0) {
            btnBack.setVisibility(View.GONE);
        } else {
            btnBack.setVisibility(View.VISIBLE);
        }
    }
}