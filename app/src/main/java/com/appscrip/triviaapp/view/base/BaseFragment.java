package com.appscrip.triviaapp.view.base;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;

import com.appscrip.triviaapp.prefrences.PreferenceManager;
import com.appscrip.triviaapp.utils.bridges.PreferenceBridge;

public class BaseFragment extends Fragment implements PreferenceBridge {

    public ActionBar getSupportedActionBar() {
        return ((BaseActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public PreferenceManager getPreferenceManager() {
        try {
            return ((BaseActivity) getActivity()).getPreferenceManager();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}

