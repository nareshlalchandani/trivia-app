package com.appscrip.triviaapp.view.activity;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.adapter.HistoryAdapter;
import com.appscrip.triviaapp.database.DatabaseHelper;
import com.appscrip.triviaapp.database.Tables.SessionTable;
import com.appscrip.triviaapp.model.SessionModel;
import com.appscrip.triviaapp.view.base.BaseActivity;

import java.util.List;

public class HistoryActivity extends BaseActivity {

    RecyclerView recyclerView;
    TextView txtErrorMessage;

    HistoryAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        //Init views
        init();

        //Load data to view
        loadData();
    }

    private void init() {

        setUpActionBarWithUpButton();//Enable back button
        setActionBarTitle("History");//Activity title

        txtErrorMessage = findViewById(R.id.txt_error_message);

        recyclerView = findViewById(R.id.recycler_view);
        adapter = new HistoryAdapter(this);
        recyclerView.setAdapter(adapter);

        txtErrorMessage.setVisibility(View.GONE);

    }

    private void loadData() {

        //Fetch data from table and update adapter
        List<SessionModel> list = SessionTable.getSession(DatabaseHelper.getInstance(this));
        adapter.add(list);

        //Update UI
        updateErrorUI();
    }

    /**
     * Update UI when adapter is empty
     */
    private void updateErrorUI() {

        if (adapter.isEmpty()) {
            txtErrorMessage.setVisibility(View.VISIBLE);//Make visible
            txtErrorMessage.setText(R.string.ui_message_ho_history);
        } else {
            txtErrorMessage.setVisibility(View.GONE);//Make in visible
        }
    }
}
