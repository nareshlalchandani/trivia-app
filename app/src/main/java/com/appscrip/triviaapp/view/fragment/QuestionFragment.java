package com.appscrip.triviaapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.adapter.MultipleSelectAdapter;
import com.appscrip.triviaapp.adapter.SingleSelectAdapter;
import com.appscrip.triviaapp.model.AnswerModel;
import com.appscrip.triviaapp.model.QuestionModel;
import com.appscrip.triviaapp.view.base.BaseFragment;
import com.google.gson.Gson;

public class QuestionFragment extends BaseFragment {

    public static final String TAG = QuestionFragment.class.getSimpleName();
    public static final String DATA_MODEL = "DataModel";

    TextView txtUserName;
    TextView txtQuestion;
    android.support.v7.widget.RecyclerView recyclerOptions;

    QuestionModel questionModel;

    SingleSelectAdapter singleSelectAdapter;
    MultipleSelectAdapter multipleSelectAdapter;

    /**
     * Get current fragment instance
     *
     * @param data, model data in form of string
     */
    public static QuestionFragment newInstance(String data) {

        //Create instance
        QuestionFragment fragment = new QuestionFragment();
        Bundle bundle = new Bundle();
        bundle.putString(DATA_MODEL, data);//Pass data in bundle
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_question, container, false);
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Get data from bundle object
        String datString = getArguments().getString(DATA_MODEL);
        questionModel = new Gson().fromJson(datString, QuestionModel.class);

        //Init Views
        txtUserName = rootView.findViewById(R.id.txt_user_name);
        txtQuestion = rootView.findViewById(R.id.txt_question);
        recyclerOptions = rootView.findViewById(R.id.recycler_options);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Set data into vies
        txtUserName.setText("Hi, " + getPreferenceManager().getName());
        txtQuestion.setText("Q " + questionModel.getQuestionId() + "." + questionModel.getQuestion());

        //Check condition for multiple type of questions
        if (questionModel.getType().equalsIgnoreCase("Single")) {

            singleSelectAdapter = new SingleSelectAdapter(getContext());
            singleSelectAdapter.add(questionModel.getOptions());
            recyclerOptions.setAdapter(singleSelectAdapter);

        } else if (questionModel.getType().equalsIgnoreCase("Multiple")) {

            multipleSelectAdapter = new MultipleSelectAdapter(getContext());
            multipleSelectAdapter.add(questionModel.getOptions());
            recyclerOptions.setAdapter(multipleSelectAdapter);
        }
    }

    /**
     * get answer based on loaded view type
     *
     * @return answer model
     */
    public AnswerModel getAnswer() {

        String answer = "";

        //Check condition for multiple type of questions
        if (questionModel.getType().equalsIgnoreCase("Single")) {

            answer = singleSelectAdapter.getAnswer();

        } else if (questionModel.getType().equalsIgnoreCase("Multiple")) {
            answer = multipleSelectAdapter.getAnswer();
        }

        //Prepare answer model class
        AnswerModel model = new AnswerModel();
        model.setQuestionId(questionModel.getQuestionId());
        model.setQuestion(questionModel.getQuestion());
        model.setAnswer(answer);

        return model;
    }
}