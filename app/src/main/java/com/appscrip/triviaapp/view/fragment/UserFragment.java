package com.appscrip.triviaapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.database.DatabaseHelper;
import com.appscrip.triviaapp.database.Tables.SessionTable;
import com.appscrip.triviaapp.model.SessionModel;
import com.appscrip.triviaapp.utils.DateOperation;
import com.appscrip.triviaapp.utils.FragmentUtil;
import com.appscrip.triviaapp.utils.ToastUtils;
import com.appscrip.triviaapp.view.base.BaseFragment;

public class UserFragment extends BaseFragment {

    public static final String TAG = UserFragment.class.getSimpleName();

    EditText edtName;
    Button btnContinue;

    public static UserFragment newInstance() {

        //Create instance
        UserFragment fragment = new UserFragment();

        //Create bundle instance
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user, container, false);
    }

    @Override
    public void onViewCreated(View rootView, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        //Init views
        edtName = rootView.findViewById(R.id.edt_name);
        btnContinue = rootView.findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }

    private void validate() {

        //Check username is empty or not, if yes then return.
        if (TextUtils.isEmpty(edtName.getText())) {
            ToastUtils.shortToast("Enter user name");
            return;
        }

        //Create session class to insert into session table
        SessionModel model = new SessionModel();
        model.setUserName(edtName.getText().toString());
        model.setStartDate(DateOperation.getCurrentMs());

        //Insert data into session table
        int sessionId = SessionTable.createSession(model, DatabaseHelper.getInstance(getContext()));

        //Load fragment
        FragmentUtil.replaceMainScreenFragment(getContext(), GameFragment.newInstance(sessionId), true);

        //Store username in preference for further use
        getPreferenceManager().setName(edtName.getText().toString());

        edtName.setText(null);//Clear input field
    }
}