package com.appscrip.triviaapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.view.base.BaseActivity;

public class SplashActivity extends BaseActivity {

    public static final String TAG = SplashActivity.class.getSimpleName();

    private static final int INTERVAL_TIME = 1500;// Duration to visible splash screen

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);

        launchScreen();//launch main activity
    }

    private void launchScreen() {

        //Set the splash screen duration, after route to the right activities
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);

                //Finish current activity
                finish();

            }
        }, INTERVAL_TIME);
    }
}