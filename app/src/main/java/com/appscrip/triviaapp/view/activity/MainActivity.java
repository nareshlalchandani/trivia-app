package com.appscrip.triviaapp.view.activity;

import android.os.Bundle;

import com.appscrip.triviaapp.R;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.appscrip.triviaapp.utils.FragmentUtil;
import com.appscrip.triviaapp.utils.ToastUtils;
import com.appscrip.triviaapp.view.base.BaseActivity;
import com.appscrip.triviaapp.view.fragment.UserFragment;

public class MainActivity extends BaseActivity implements FragmentManager.OnBackStackChangedListener {

    public static final String TAG = MainActivity.class.getSimpleName();

    boolean doubleBackToExitPressedOnce = false;

    MenuItem menuItemHistory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Register back stack change listener for fragment
        getSupportFragmentManager().addOnBackStackChangedListener(this);

        //Load user detail fragment
        FragmentUtil.replaceMainScreenFragment(this, UserFragment.newInstance(), false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_history, menu);

        //Get reference
        menuItemHistory = menu.findItem(R.id.action_history);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = null;

        switch (item.getItemId()) {

            // Action to launch history activity
            case R.id.action_history:

                intent = new Intent(this, HistoryActivity.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {

        //Checking back stack entry available of not, If yes the perform back operation
        if (getSupportFragmentManager() != null && getSupportFragmentManager().getBackStackEntryCount() > 0) {

            //Transfer to  previous screen
            getSupportFragmentManager().popBackStack();
            return;
        }

        //Checking condition to get app close
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        ToastUtils.shortToast("Please click BACK again to exit");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public void onBackStackChanged() {

        //Check status
        if (getSupportFragmentManager() != null) {

            //Get back stack count
            int index = getSupportFragmentManager().getBackStackEntryCount();

            //LogUtility.printErrorMsg(TAG, "BackStackEntryCount : " + index);

            if (index > 0) {

                //Hide history menu
                if (menuItemHistory != null) {
                    menuItemHistory.setVisible(false);
                }

            } else {

                //Show history menu
                if (menuItemHistory != null) {
                    menuItemHistory.setVisible(true);
                }
            }
        }
    }
}