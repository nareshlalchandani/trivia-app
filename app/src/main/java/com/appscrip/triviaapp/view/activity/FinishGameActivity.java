package com.appscrip.triviaapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.adapter.FinishGameAdapter;
import com.appscrip.triviaapp.database.DatabaseHelper;
import com.appscrip.triviaapp.database.Tables.AnswerTable;
import com.appscrip.triviaapp.database.Tables.SessionTable;
import com.appscrip.triviaapp.model.AnswerModel;
import com.appscrip.triviaapp.utils.LogUtility;
import com.appscrip.triviaapp.utils.ToastUtils;
import com.appscrip.triviaapp.view.base.BaseActivity;
import com.google.gson.Gson;

import java.util.List;

public class FinishGameActivity extends BaseActivity {

    public static final String TAG = FinishGameActivity.class.getSimpleName();

    public static final String SESSION_ID = "SessionId";

    private DatabaseHelper databaseHelper;

    TextView txtName;
    RecyclerView recyclerView;
    Button btnFinish;
    FinishGameAdapter adapter;

    int sessionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_game);

        //Get session value from bundle
        sessionId = getIntent().getIntExtra(SESSION_ID, 0);

        if (sessionId == 0) {
            ToastUtils.shortToast(R.string.toast_missing_parameter);
            finish();
            return;
        }

        //Create database helper object
        databaseHelper = DatabaseHelper.getInstance(this);

        init();//Init Views

        loadData();//Load data
    }

    private void init() {

        setUpActionBarWithUpButton();//Enable back button
        setActionBarTitle("Summery");//Activity title

        //Init vies
        txtName = findViewById(R.id.txt_name);
        recyclerView = findViewById(R.id.recycler_answer);
        btnFinish = findViewById(R.id.btn_finish);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Update end time
                SessionTable.updateEndTime(databaseHelper);

                //launch main activity by clear old task
                Intent intent = new Intent(FinishGameActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }

    //Load data into views
    private void loadData() {

        //Fetch data from answer table
        List<AnswerModel> answerModels = AnswerTable.getHistory(sessionId, databaseHelper);

        //LogUtility.printDebugMsg(TAG, "Answers : " + new Gson().toJson(answerModels));

        txtName.setText(getPreferenceManager().getName());//Set user name

        //Load answer recycler view
        adapter = new FinishGameAdapter(this);
        adapter.add(answerModels);
        recyclerView.setAdapter(adapter);
    }
}
