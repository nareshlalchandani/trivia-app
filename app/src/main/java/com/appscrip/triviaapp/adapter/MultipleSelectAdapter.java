package com.appscrip.triviaapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.appscrip.triviaapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by w7 on 12/12/2016.
 */

public class MultipleSelectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> selectedOptions = new ArrayList<>();

    private List<String> list;
    private Context context;

    public MultipleSelectAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_checkbox, parent, false);
        return new VHMultipleSelect(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final VHMultipleSelect vh = (VHMultipleSelect) holder;
        vh.checkbox.setText(getObject(position));

        vh.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (vh.checkbox.isChecked()) {
                    vh.checkbox.setChecked(false);
                    selectedOptions.remove(getObject(position));

                } else {
                    vh.checkbox.setChecked(true);
                    selectedOptions.add(getObject(position));
                }
            }
        });
    }

    class VHMultipleSelect extends RecyclerView.ViewHolder {

        CheckBox checkbox;
        View view;

        VHMultipleSelect(final View rootView) {
            super(rootView);

            checkbox = rootView.findViewById(R.id.checkbox);
            view = rootView;
        }
    }

    public void add(List<String> models) {

        if (models == null) {
            return;
        }

        list.addAll(models);

        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public String getObject(int position) {
        return list.get(position);
    }

    public String getAnswer() {

        if (selectedOptions.isEmpty()) {
            return null;

        } else {

            StringBuilder answer = new StringBuilder();

            for (int i = 0; i < selectedOptions.size(); i++) {

                answer.append(selectedOptions.get(i));

                if (i != selectedOptions.size() - 1)
                    answer.append(",");
            }

            return answer.toString();
        }
    }
}
