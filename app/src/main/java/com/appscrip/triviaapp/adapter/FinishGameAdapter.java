package com.appscrip.triviaapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.model.AnswerModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by w7 on 12/12/2016.
 */

public class FinishGameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<AnswerModel> list;
    private Context context;

    public FinishGameAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_finish_game, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ViewHolder vh = (ViewHolder) holder;

        AnswerModel model = getObject(position);

        vh.txtQuestion.setText(model.getQuestion());
        vh.txtAnswer.setText(model.getAnswer());
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtQuestion;
        TextView txtAnswer;

        ViewHolder(final View rootView) {
            super(rootView);

            txtQuestion = rootView.findViewById(R.id.txt_question);
            txtAnswer = rootView.findViewById(R.id.txt_answer);
        }
    }

    public void add(List<AnswerModel> models) {

        if (models == null) {
            return;
        }

        list.addAll(models);

        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public AnswerModel getObject(int position) {
        return list.get(position);
    }
}
