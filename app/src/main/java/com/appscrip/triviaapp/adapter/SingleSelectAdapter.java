package com.appscrip.triviaapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.appscrip.triviaapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by w7 on 12/12/2016.
 */

public class SingleSelectAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> list;
    private Context context;

    private int selected = -1;

    public SingleSelectAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_radio, parent, false);
        return new VHSingleSelect(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        VHSingleSelect vh = (VHSingleSelect) holder;
        vh.radio.setText(getObject(position));

        vh.radio.setChecked(selected == position);

        vh.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected = position;
                notifyDataSetChanged();
            }
        });
    }

    class VHSingleSelect extends RecyclerView.ViewHolder {

        RadioButton radio;
        View view;

        VHSingleSelect(final View rootView) {
            super(rootView);

            radio = rootView.findViewById(R.id.radio);
            view = rootView;
        }
    }

    public void add(List<String> models) {

        if (models == null) {
            return;
        }

        list.addAll(models);

        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public String getObject(int position) {
        return list.get(position);
    }

    public String getAnswer() {
        if (selected == -1) {
            return null;
        } else {
            return list.get(selected);
        }
    }
}
