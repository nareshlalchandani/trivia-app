package com.appscrip.triviaapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appscrip.triviaapp.R;
import com.appscrip.triviaapp.database.DatabaseHelper;
import com.appscrip.triviaapp.database.Tables.AnswerTable;
import com.appscrip.triviaapp.model.AnswerModel;
import com.appscrip.triviaapp.model.SessionModel;
import com.appscrip.triviaapp.utils.DateOperation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by w7 on 12/12/2016.
 */

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<SessionModel> list;
    private Context context;
    DatabaseHelper databaseHelper;

    public HistoryAdapter(Context context) {
        this.list = new ArrayList<>();
        this.context = context;
        this.databaseHelper = DatabaseHelper.getInstance(context);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_history, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        ViewHolder vh = (ViewHolder) holder;
        SessionModel model = getObject(position);

        vh.txtGameNo.setText("Game : " + model.getSessionId());
        vh.txtDate.setText(DateOperation.getDateFromMs(Long.valueOf(model.getStartDate())));
        vh.txtName.setText("Name : " + model.getUserName());

        List<AnswerModel> answerModels = AnswerTable.getHistory(model.getSessionId(), databaseHelper);

        FinishGameAdapter adapter = new FinishGameAdapter(context);
        adapter.add(answerModels);
        vh.recyclerAnswer.setAdapter(adapter);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtGameNo;
        TextView txtDate;
        TextView txtName;
        RecyclerView recyclerAnswer;

        ViewHolder(final View rootView) {
            super(rootView);

            txtGameNo = rootView.findViewById(R.id.txt_game_no);
            txtDate = rootView.findViewById(R.id.txt_date);
            txtName = rootView.findViewById(R.id.txt_name);
            recyclerAnswer = rootView.findViewById(R.id.recycler_answer);
        }
    }

    public void add(List<SessionModel> models) {

        if (models == null) {
            return;
        }

        list.addAll(models);

        notifyDataSetChanged();
    }

    public void clear() {
        list.clear();
    }

    public boolean isEmpty() {
        return list.isEmpty();
    }

    public SessionModel getObject(int position) {
        return list.get(position);
    }
}
