
package com.appscrip.triviaapp.database.Tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.appscrip.triviaapp.model.AnswerModel;
import com.appscrip.triviaapp.utils.DateOperation;
import com.appscrip.triviaapp.utils.LogUtility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class AnswerTable {

    public static final String TABLE_NAME = AnswerTable.class.getSimpleName();

    private static final String FIELD_AUTO_ID = "AutoId";
    private static final String FIELD_SESSION_ID = "SessionId";
    private static final String FIELD_QUESTION_ID = "QuestionId";
    private static final String FIELD_QUESTION = "Question";
    private static final String FIELD_ANSWER = "Answer";

    //Create table query
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("

            + FIELD_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + FIELD_SESSION_ID + " INTEGER,"
            + FIELD_QUESTION_ID + " INTEGER ,"
            + FIELD_QUESTION + " TEXT ,"
            + FIELD_ANSWER + " TEXT "
            + " )";

    /**
     * Create table
     *
     * @param db, database reference
     */
    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void saveAnswer(int sessionId, AnswerModel model, SQLiteOpenHelper db) {

        AnswerModel answerModel = getAnswerById(sessionId, model.getQuestionId(), db);

        if (answerModel == null) {
            insert(sessionId, model, db);//Insert
        } else {
            updateAnswer(sessionId, model.getQuestionId(), model.getAnswer(), db);//Update
        }
    }

    /**
     * * insert records in table
     *
     * @param db, database reference
     */
    public static void insert(int sessionId, AnswerModel model, SQLiteOpenHelper db) {

        SQLiteDatabase database = null;

        try {

            database = db.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(FIELD_SESSION_ID, sessionId);
            values.put(FIELD_QUESTION_ID, model.getQuestionId());
            values.put(FIELD_QUESTION, model.getQuestion());
            values.put(FIELD_ANSWER, model.getAnswer());

            long count = database.insert(TABLE_NAME, null, values);
            //LogUtility.printDebugMsg("Count : " + count);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {

            if (database != null) {
                database.close();
            }
        }
    }

    /**
     * Static method to insert records, Can be call from class name
     *
     * @param db,        database reference
     * @param sessionId, session id, ite is auto id of {@SessionTable}
     * @return List<AnswerModel>, history list
     */
    public static List<AnswerModel> getHistory(int sessionId, SQLiteOpenHelper db) {

        SQLiteDatabase database = db.getReadableDatabase();

        List<AnswerModel> list = new ArrayList<>();

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_SESSION_ID + "=" + sessionId, null);

        while (cursor.moveToNext()) {

            int autoId = cursor.getInt(cursor.getColumnIndex(FIELD_QUESTION_ID));
            int questionId = cursor.getInt(cursor.getColumnIndex(FIELD_QUESTION_ID));
            String question = cursor.getString(cursor.getColumnIndex(FIELD_QUESTION));
            String answer = cursor.getString(cursor.getColumnIndex(FIELD_ANSWER));

            AnswerModel model = new AnswerModel();

            model.setQuestionId(questionId);
            model.setQuestion(question);
            model.setAnswer(answer);

            list.add(model);
        }

        cursor.close();

        return list;
    }

    /**
     * Static method to insert records, Can be call from class name
     *
     * @param db,          database reference
     * @param _sessionId,  session id
     * @param _questionId, question id
     * @return AnswerModel, history list
     */
    public static AnswerModel getAnswerById(int _sessionId, int _questionId, SQLiteOpenHelper db) {

        SQLiteDatabase database = db.getReadableDatabase();

        AnswerModel model = null;

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_SESSION_ID + "=" + _sessionId + " AND " + FIELD_QUESTION_ID + "=" + _questionId, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            int autoId = cursor.getInt(cursor.getColumnIndex(FIELD_QUESTION_ID));
            int questionId = cursor.getInt(cursor.getColumnIndex(FIELD_QUESTION_ID));
            String question = cursor.getString(cursor.getColumnIndex(FIELD_QUESTION));
            String answer = cursor.getString(cursor.getColumnIndex(FIELD_ANSWER));

            model = new AnswerModel();

            model.setQuestionId(questionId);
            model.setQuestion(question);
            model.setAnswer(answer);
        }

        cursor.close();

        return model;
    }

    /**
     * update answer
     *
     * @param db,          database reference
     * @param _sessionId,  session id
     * @param _questionId, question id
     */
    public static void updateAnswer(int _sessionId, int _questionId, String answer, SQLiteOpenHelper db) {

        try {

            SQLiteDatabase database = db.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(FIELD_ANSWER, answer);

            database.update(TABLE_NAME, values, FIELD_SESSION_ID + "=" + _sessionId + " AND " + FIELD_QUESTION_ID + "=" + _questionId, null);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    //Clear table
    public static void clearTable(SQLiteOpenHelper db) {

        String query = "DELETE FROM " + TABLE_NAME;

        db.getWritableDatabase().execSQL(query);
    }

    /**
     * Static method to insert records, Can be call from class name
     *
     * @param db, database reference
     * @return int, no of records present in table
     */
    public static int count(SQLiteOpenHelper db) {

        int count = 0;

        String select_Qry = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase database = db.getReadableDatabase();

        Cursor cursor = database.rawQuery(select_Qry, null);

        if (cursor != null && cursor.getCount() > 0) {

            count = cursor.getCount();
            cursor.close();
        }

        return count;
    }
}

