package com.appscrip.triviaapp.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.appscrip.triviaapp.database.Tables.AnswerTable;
import com.appscrip.triviaapp.database.Tables.SessionTable;
import com.appscrip.triviaapp.utils.AppConstant;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = AppConstant.PROJECT_NAME + ".db";

    private static int DATABASE_VERSION = 1;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHelper getInstance(Context context) {
        return new DatabaseHelper(context);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        SessionTable.onCreate(db);
        AnswerTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion <= 1) {

            db.execSQL("DROP TABLE IF EXISTS " + SessionTable.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + AnswerTable.TABLE_NAME);

            SessionTable.onCreate(db);
            AnswerTable.onCreate(db);
        }
    }
}

