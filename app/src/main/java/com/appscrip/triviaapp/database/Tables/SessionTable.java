package com.appscrip.triviaapp.database.Tables;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.appscrip.triviaapp.model.SessionModel;
import com.appscrip.triviaapp.utils.DateOperation;
import com.appscrip.triviaapp.utils.LogUtility;

import java.util.ArrayList;
import java.util.List;

public class SessionTable {

    public static final String TABLE_NAME = SessionTable.class.getSimpleName();

    private static final String FIELD_AUTO_ID = "AutoId";
    private static final String FIELD_USER_NAME = "UserName";
    private static final String FIELD_START_DATE = "StartDate";
    private static final String FIELD_END_DATE = "EndDate";

    //Create table query
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("

            + FIELD_AUTO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
            + FIELD_USER_NAME + " TEXT ,"
            + FIELD_START_DATE + " TEXT ,"
            + FIELD_END_DATE + " TEXT "
            + " )";

    /**
     * Create table
     *
     * @param db, database reference
     */
    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static int createSession(SessionModel model, SQLiteOpenHelper db) {

        SQLiteDatabase database = null;
        long lastId = 0;

        try {

            database = db.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(FIELD_USER_NAME, model.getUserName());
            values.put(FIELD_START_DATE, model.getStartDate());

            lastId = database.insert(TABLE_NAME, null, values);
            LogUtility.printDebugMsg("Count : " + lastId);

        } catch (Exception e) {
            e.printStackTrace();

        } finally {

            if (database != null) {
                database.close();
            }
        }

        return (int) lastId;
    }

    /**
     * insert records in table
     *
     * @param db, database reference
     * @return List<SessionModel>, session list
     */
    public static List<SessionModel> getSession(SQLiteOpenHelper db) {

        SQLiteDatabase database = db.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY " + FIELD_AUTO_ID + " DESC", null);

        List<SessionModel> list = new ArrayList<>();

        while (cursor.moveToNext()) {

            int autoId = cursor.getInt(cursor.getColumnIndex(FIELD_AUTO_ID));
            String userName = cursor.getString(cursor.getColumnIndex(FIELD_USER_NAME));
            String startDate = cursor.getString(cursor.getColumnIndex(FIELD_START_DATE));
            String endDate = cursor.getString(cursor.getColumnIndex(FIELD_END_DATE));

            SessionModel model = new SessionModel();
            model.setSessionId(autoId);
            model.setUserName(userName);
            model.setStartDate(startDate);
            model.setEndDate(endDate);

            list.add(model);
        }

        cursor.close();

        return list;
    }

    public static SessionModel getSessionById(int sessionId, SQLiteOpenHelper db) {

        SQLiteDatabase database = db.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + FIELD_AUTO_ID + "=" + sessionId, null);

        SessionModel model = null;

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();

            int autoId = cursor.getInt(cursor.getColumnIndex(FIELD_AUTO_ID));
            String userName = cursor.getString(cursor.getColumnIndex(FIELD_USER_NAME));
            String startDate = cursor.getString(cursor.getColumnIndex(FIELD_START_DATE));
            String endDate = cursor.getString(cursor.getColumnIndex(FIELD_END_DATE));

            model = new SessionModel();

            model.setSessionId(autoId);
            model.setUserName(userName);
            model.setStartDate(startDate);
            model.setEndDate(endDate);

        }

        cursor.close();

        return model;
    }

    /**
     * Update end time, when user submit final game
     *
     * @param db, database reference
     */
    public static void updateEndTime(SQLiteOpenHelper db) {

        try {

            SQLiteDatabase database = db.getWritableDatabase();

            ContentValues values = new ContentValues();

            values.put(FIELD_END_DATE, DateOperation.getCurrentMs());

            database.update(TABLE_NAME, values, null, null);

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    //Clear table
    public static void clearTable(SQLiteOpenHelper db) {

        String query = "DELETE FROM " + TABLE_NAME;

        db.getWritableDatabase().execSQL(query);
    }

    /**
     * Static method to insert records, Can be call from class name
     *
     * @param db, database reference
     * @return int, no of records present in table
     */
    public static int count(SQLiteOpenHelper db) {

        int count = 0;

        String select_Qry = "SELECT * FROM " + TABLE_NAME;

        SQLiteDatabase database = db.getReadableDatabase();

        Cursor cursor = database.rawQuery(select_Qry, null);

        if (cursor != null && cursor.getCount() > 0) {

            count = cursor.getCount();
            cursor.close();
        }

        return count;
    }
}

