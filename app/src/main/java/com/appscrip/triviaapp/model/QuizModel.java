package com.appscrip.triviaapp.model;

import java.util.List;

public class QuizModel {

    private List<QuestionModel> data;

    public List<QuestionModel> getData() {
        return data;
    }

    public void setData(List<QuestionModel> data) {
        this.data = data;
    }
}
