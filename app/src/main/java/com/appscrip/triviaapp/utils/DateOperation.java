package com.appscrip.triviaapp.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by naresh chandani
 */
public class DateOperation {

    /**
     * Get current millisecond
     *
     * @return ,  current date in ms
     */
    public static String getCurrentMs() {

        return String.valueOf(new Date().getTime());
    }

    /**
     * Get date in given format
     *
     * @return , get date in given format
     */
    public static String getDateFromMs(Long time) {

        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM hh:mm a", Locale.US);

        String str = null;

        try {
            str = outputFormat.format(new Date(time));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }
}
