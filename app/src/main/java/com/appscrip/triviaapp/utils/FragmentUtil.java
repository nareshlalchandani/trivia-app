package com.appscrip.triviaapp.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.appscrip.triviaapp.R;

/**
 * Created by naresh chandani
 */
public class FragmentUtil {

    private static final String TAG = FragmentUtil.class.getSimpleName();

    /**
     * Load main activity fragment
     *
     * @param context,     pass activity instance
     * @param fragment,    fragment to be load
     * @param isBackAllow, if true means add in back stack,else don't
     */
    public static void replaceMainScreenFragment(Context context, Fragment fragment, boolean isBackAllow) {

        try {
            //Inst of fragment manager from context
            FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

            //Inst of fragment transaction
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frame_layout, fragment);

            //Checking back stack status
            if (isBackAllow) {
                fragmentTransaction.addToBackStack(Fragment.class.getSimpleName());
            }

            //Commit transaction
            fragmentTransaction.commit();

        } catch (ClassCastException e) {
            LogUtility.printDebugMsg(TAG, "Can't get fragment manager");
        }

    }
}
