package com.appscrip.triviaapp.utils;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by naresh chandani
 */
public class JsonUtil {

    /**
     * Convert json data to string
     *
     * @param context,  pass activity instance
     * @param fileName, file name with extension
     */
    public static String loadJSONFromAsset(Context context, String fileName) {

        String json = null;
        try {
            InputStream is = context.getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
