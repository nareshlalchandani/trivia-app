package com.appscrip.triviaapp.utils.bridges;


import com.appscrip.triviaapp.prefrences.PreferenceManager;

/**
 * Created by naresh chandani
 */
public interface PreferenceBridge {

    PreferenceManager getPreferenceManager();
}
